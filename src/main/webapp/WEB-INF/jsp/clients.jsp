<%@include file="shared/navigation.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="shared/navigation.jsp"%>

<table>
	<tr>
		<td align="center"><img src="static/block.jpg"></td>
	</tr>
	<tr>
		<td><table>
				<thead>
					<tr>
						<th>id</th>
						<th>login</th>
						<th>password</th>
						<th>blocked?</th>
						<th>Orders</th>
						<th>block</th>
						<th>unblock</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${clients}" var="item">
						<tr>
							<td>${item.id}</td>
							<td>${item.login}</td>
							<td>${item.password}</td>
							<td>${item.block}</td>
							<td><a href="${orderUrl}">Check orders</a></td>
							<td><a href="/me?block=${item.id}"> block </a></td>
							<td><a href="/me?unblock=${item.id}"> unblock </a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table></td>
	</tr>
	<tr>
		<td align="center"><a href="/logout">Logout</a></td>
	</tr>
</table>

<%@include file="shared/footer.jsp"%>