<%@include file="header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<nav>
	<ul>
		<li><a href="../index">Home</a></li>
		<li><c:if test="${empty user}">
				<a href="/register">Register</a>
			</c:if> 
			<c:if test="${not empty user}">
				<a href="/orders">Orders</a>
		    </c:if></li>
		<li>
			<c:if test="${empty user}">
				<a href="/login">Login</a>
			</c:if> 
			<c:if test="${not empty user}">
				<a href="/me">${user.login}</a>
		    </c:if>
		</li>
		<li><a href="../products">Products</a></li>
	</ul>
</nav>
