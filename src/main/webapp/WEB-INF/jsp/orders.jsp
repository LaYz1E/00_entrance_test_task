<%@include file="shared/navigation.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${user.isAdmin()}">
<table>
	<thead>
		<tr>
			<th>X</th>
			<th>orderer</th>
			<th>product</th>
			<th>price</th>
			<th>paid</th>
			<th>confirm</th>
			<th>reject</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${orders}" var="item">
			<tr>
				<td><a href="/orders?delete=${item.id}">[X]</a></td>
				<td>${item.client_id}</td>
				<td>${item.name}</td>
				<td>${item.paid}</td>
				<td><a href="/orders?pay=${item.id}">Paid</a></td>
				<td><a href="/orders?unpay=${item.id}">Not Paid</a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</c:if>
<c:if test="${user.isAdmin()==false}">
<table>
	<thead>
		<tr>
			<th>product_id</th>
			<th>paid</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${orders}" var="item">
			<tr>
				<td>${item.name}</td>
				<td>${item.paid}</td>
				<td></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</c:if>




<%@include file="shared/footer.jsp"%>