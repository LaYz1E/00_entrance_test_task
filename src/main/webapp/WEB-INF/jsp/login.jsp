<%@include file="shared/navigation.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<table>

	<c:if test="${not empty error}">
		<tr>
			<td>
				<p>
					<font color="red" size="4" face="sans-serif"> Bad login or
						password</font>
				</p>
			</td>
		</tr>
	</c:if>
	<tr>
		<td>
			<form name="login" method="post" action="/login">
				<p>Login</p>
				<input type="text" name="login" required="required">
				<p>Password</p>
				<input type="password" name="password" required="required">
				<p>
					<input type="submit" value="Log in">
				</p>
			</form>
		</td>
	</tr>
</table>
<%@include file="shared/footer.jsp"%>