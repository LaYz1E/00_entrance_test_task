<%@include file="shared/navigation.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>
function showPopDesc(id) {
    var popup = document.getElementById("descPop"+id);
    popup.classList.toggle("show");
}

function shopPopChange(id){
	var popup = document.getElementById("changePop"+id);
    popup.classList.toggle("show");
}

</script>
<c:if test="${user.isAdmin()==false}">
	<table>
		<thead>
			<tr>
				<th><img src="static/prod.jpg"></th>
			</tr>
			<tr>
				<th>name</th>
				<th></th>
				<th>price</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${products}" var="item">
				<tr>
					<td><div class="popup" onclick="showPopDesc(${item.id})">
							${item.name}  <span class="popuptext" id="descPop${item.id}">
								${item.name} ${item.specs} ${item.price} </span>
						</div></td>
					<td>${item.price}</td>
					<td><a href="/products?order=${item.id}">Order it!</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${user.isAdmin()}">
	<table>
		<tr>
			<td>
				<table>
					<thead>
						<tr>
							<img src="static/prod.jpg">
						</tr>
						<tr>
							<th>x</th>
							<th>name</th>
							<th>price</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${products}" var="item">
							<tr>
								<td><a href="/products?delete=${item.id}">[X]</a>
								<td><div class="popup" onclick="showPopDesc(${item.id})">
										${item.name} <span class="popuptext" id="descPop${item.id}">
											${item.name} ${item.specs} ${item.price} </span>
									</div></td>
								<td>${item.price}</td>
								<td><div class="popup" onclick="shopPopChange(${item.id})">
										change it <span class="popuptext" id="changePop${item.id}">
											<form name="editProduct" method="post"
												action="/products?id=${item.id}">
												<p>Name</p>
												<input type="text" name="name" required="required">
												<p>Price</p>
												<input type="number" name="price" min="0" required="required">
												<p>Specs</p>
												<input type="text" name="specs">
												<p>
													<input type="submit" value="Edit">
												</p>
											</form>
										</span>
									</div></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<form name="product" method="post" action="/products">
					<input type="text" name="name" required="required"> <input
						type="number" name="price" required="required"> <input
						type="submit" value="add product">
				</form>
			</td>
		</tr>
	</table>
</c:if>

<%@include file="shared/footer.jsp"%>