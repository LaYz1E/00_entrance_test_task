package com.WebShop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.WebShop.dao.ProductDAO;
import com.WebShop.dao.impl.ProductDAOImpl;
import com.WebShop.domain.Product;

@Service
public class ProductService {

	private final ProductDAO productDAO;

	@Autowired
	public ProductService(ProductDAOImpl clDAO) {
		this.productDAO = clDAO;
	}

	public Product save(Product inc) {
		productDAO.save(inc);
		return inc;
	}

	public List<Product> findAll() {
		return productDAO.findAll();
	}

	public Product findByID(int id) {
		return productDAO.findByID(id);
	}

	public void updateByID(int id, Product inc) {
		productDAO.updateByID(id, inc);
	}

	public void delete(int id) {
		productDAO.deleteByID(id);
	}

}
