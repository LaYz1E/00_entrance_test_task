package com.WebShop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.WebShop.dao.OrderDAO;
import com.WebShop.dao.impl.OrderDAOImpl;
import com.WebShop.domain.Order;

@Service
public class OrderService {
	OrderDAO orderDAO;

	@Autowired
	public OrderService(OrderDAOImpl orDAO) {
		this.orderDAO = orDAO;

	}

	public Order save(Order inc) {
		return orderDAO.save(inc);
	}

	public List<Order> findAll() {
		return orderDAO.findAll();
	}

	public List<Order> findByClientID(int id) {
		return orderDAO.findByClientID(id);
	}

	public Order findByID(int id) {
		return orderDAO.findByID(id);
	}

	public void updateByID(int id, Order inc) {
		orderDAO.updateByID(id, inc);
	}

	public void setPayByID(int id, boolean pay) {
		orderDAO.setPayByID(id, pay);
	}

	public void deleteByID(int id) {
		orderDAO.deleteByID(id);
	}

}