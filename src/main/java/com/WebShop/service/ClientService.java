package com.WebShop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.WebShop.dao.ClientDAO;
import com.WebShop.dao.impl.ClientDAOImpl;
import com.WebShop.domain.Client;

@Service
public class ClientService {

	private final ClientDAO clientDAO;

	@Autowired
	public ClientService(ClientDAOImpl clDAO) {
		this.clientDAO = clDAO;
	}

	public Client save(Client entity) {
		clientDAO.save(entity);
		return entity;
	}

	public List<Client> findAll() {
		return clientDAO.findAll();
	}

	public Client findbyID(int id) {
		return clientDAO.findByID(id);
	}

	public Client findByLogin(String login) {
		return clientDAO.findByLogin(login);
	}

	public void updateByID(int id, Client inc) {
		clientDAO.updateByID(id, inc);
	}

	public void delete(int id) {
		clientDAO.deleteByID(id);
	}

	public Client authenticate(Client user) {
		Client foundUser = this.findByLogin(user.getLogin());
		if (foundUser == null) {
			return null;
		} else if (!foundUser.getPassword().equals(user.getPassword())) {
			return null;
		} else
			return foundUser;
	}

	public void setBlock(int id, boolean block) {
		clientDAO.setBlock(id, block);
	}

}
