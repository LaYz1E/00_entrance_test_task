package com.WebShop.dao;

import java.util.List;

import com.WebShop.domain.Order;

public interface OrderDAO {

	public Order save(Order inc);

	public List<Order> findAll();

	public List<Order> findByClientID(int id);

	public Order findByID(int id);

	public void updateByID(int id, Order inc);

	public void setPayByID(int id, boolean pay);

	public void deleteByID(int id);

}
