package com.WebShop.dao;

import java.util.List;

import com.WebShop.domain.Client;

public interface ClientDAO {

	public Client save(Client inc);

	public List<Client> findAll();

	public Client findByID(int id);

	public Client findByLogin(String login);

	public void updateByID(int id, Client inc);

	public void deleteByID(int id);

	public boolean isAdmin(int id);

	public void setBlock(int id, boolean block);

}
