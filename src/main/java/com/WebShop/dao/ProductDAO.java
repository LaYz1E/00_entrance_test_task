package com.WebShop.dao;

import java.util.List;

import com.WebShop.domain.Product;

public interface ProductDAO {

	public Product save(Product inc);

	public List<Product> findAll();

	public List<Product> findByName(String str);

	public Product findByID(int id);

	public void updateByID(int id, Product inc);

	public void deleteByID(int id);

}