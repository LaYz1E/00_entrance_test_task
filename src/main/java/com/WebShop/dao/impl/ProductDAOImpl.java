package com.WebShop.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.WebShop.dao.ProductDAO;
import com.WebShop.domain.Product;

@Repository
@Transactional
public class ProductDAOImpl implements ProductDAO {

	private final JdbcTemplate jdbcTemplate;
	private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public ProductDAOImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public Product save(Product inc) {
		SqlParameterSource params = new BeanPropertySqlParameterSource(inc);
		namedParameterJdbcTemplate.update("INSERT INTO PRODUCTS(name, price) VALUES(:name, :price)", params);
		return inc;
	}

	@Override
	public List<Product> findAll() {
		return jdbcTemplate.query("SELECT id, name, price, specs FROM PRODUCTS", this::mapRow);
	}

	@Override
	public Product findByID(int id) {
		return jdbcTemplate.queryForObject("SELECT id, name, price, specs FROM PRODUCTS WHERE product_id = ?",
				this::mapRow, id);
	}

	@Override
	public List<Product> findByName(String str) {
		return jdbcTemplate.query("SELECT id, name, price, specs FROM PRODUCTS WHERE product_name = ?", this::mapRow,
				str);
	}

	@Override
	public void updateByID(int id, Product inc) {
		inc.setId(id);
		SqlParameterSource params = new BeanPropertySqlParameterSource(inc);
		namedParameterJdbcTemplate.update("UPDATE PRODUCTS SET name=:name, price=:price, specs=:specs WHERE id=:id",
				params);

	}

	@Override
	public void deleteByID(int id) {
		jdbcTemplate.update("DELETE FROM PRODUCTS WHERE id = ?", id);
	}

	private Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		Product product = new Product();
		product.setId(rs.getInt("id"));
		product.setName(rs.getString("name"));
		product.setPrice(rs.getInt("price"));
		product.setSpecs(rs.getString("specs"));
		return product;
	}

}