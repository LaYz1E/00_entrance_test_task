package com.WebShop.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.WebShop.dao.ClientDAO;
import com.WebShop.domain.Client;

@Repository
@Transactional
public class ClientDAOImpl implements ClientDAO {

	private final JdbcTemplate jdbcTemplate;
	private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public ClientDAOImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public Client save(Client entity) {
		SqlParameterSource params = new BeanPropertySqlParameterSource(entity);
		namedParameterJdbcTemplate.update("INSERT INTO CLIENTS (login, password) VALUES(:login, :password)", params);
		return entity;

	}

	@Override
	public List<Client> findAll() {
		return jdbcTemplate.query("SELECT id, login, password, block, admin FROM CLIENTS", this::mapRow);
	}

	@Override
	public Client findByID(int id) {
		return jdbcTemplate.queryForObject("SELECT id, login, password, block, admin FROM CLIENTS WHERE id = ?",
				this::mapRow, id);
	}

	@Override
	public Client findByLogin(String login) {
		try {
			return jdbcTemplate.queryForObject("SELECT id, login, password, block, admin FROM CLIENTS WHERE login = ?",
					this::mapRow, login);
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public void updateByID(int id, Client inc) {
		inc.setId(id);
		SqlParameterSource params = new BeanPropertySqlParameterSource(inc);
		namedParameterJdbcTemplate.update(
				"UPDATE CLIENTS SET login=:login, password=:password, block=:block, admin=:admin WHERE id=:id", params);
	}

	@Override
	public void deleteByID(int id) {
		jdbcTemplate.update("DELETE FROM CLIENTS WHERE id = ?", id);

	}

	private Client mapRow(ResultSet rs, int rowNum) throws SQLException {
		Client client = new Client();
		client.setId(rs.getInt("id"));
		client.setLogin(rs.getString("login"));
		client.setPassword(rs.getString("password"));
		client.setBlock(rs.getBoolean("block"));
		client.setAdmin(rs.getBoolean("admin"));
		return client;
	}

	@Override
	public boolean isAdmin(int id) {
		Client client = jdbcTemplate
				.queryForObject("SELECT id, login, password, block, admin FROM CLIENTS WHERE id = ?", this::mapRow, id);
		return client.isAdmin();
	}

	@Override
	public void setBlock(int id, boolean block) {
		Client inc = new Client();
		inc.setId(id);
		inc.setBlock(block);
		SqlParameterSource params = new BeanPropertySqlParameterSource(inc);
		namedParameterJdbcTemplate.update("UPDATE CLIENTS SET block=:block WHERE id=:id", params);

	}

}
