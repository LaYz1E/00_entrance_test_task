package com.WebShop.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.WebShop.dao.OrderDAO;
import com.WebShop.domain.Order;

@Repository
@Transactional
public class OrderDAOImpl implements OrderDAO {

	private final JdbcTemplate jdbcTemplate;
	private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public OrderDAOImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public Order save(Order inc) {
		SqlParameterSource params = new BeanPropertySqlParameterSource(inc);
		namedParameterJdbcTemplate.update("INSERT INTO ORDERS (client_id, product_id) VALUES(:client_id, :product_id)",
				params);
		return inc;
	}

	@Override
	public List<Order> findAll() {
		return jdbcTemplate.query(
				"SELECT ORDERS.id, ORDERS.client_id, ORDERS.product_id, ORDERS.paid, PRODUCTS.name FROM ORDERS LEFT JOIN PRODUCTS ON ORDERS.product_id = PRODUCTS.id",
				this::mapRow2);
	}

	@Override
	public List<Order> findByClientID(int id) {
		return jdbcTemplate.query(
				"SELECT ORDERS.id, ORDERS.client_id, ORDERS.product_id, ORDERS.paid, PRODUCTS.name FROM ORDERS LEFT JOIN PRODUCTS ON ORDERS.product_id = PRODUCTS.id WHERE ORDERS.client_id = ?",
				this::mapRow2, id);
	}

	@Override
	public Order findByID(int id) {
		return jdbcTemplate.queryForObject("SELECT id, client_id, product_id, paid FROM ORDERS WHERE id = ?",
				this::mapRow, id);
	}

	@Override
	public void updateByID(int id, Order inc) {
		inc.setId(id);
		SqlParameterSource params = new BeanPropertySqlParameterSource(inc);
		namedParameterJdbcTemplate.update(
				"UPDATE ORDERS SET client_id=:client_id, product_id=:product_id, paid=:paid WHERE id=:id", params);
	}

	@Override
	public void setPayByID(int id, boolean pay) {
		jdbcTemplate.update("UPDATE ORDERS SET paid=? WHERE id=?", pay, id);
	}

	@Override
	public void deleteByID(int id) {
		jdbcTemplate.update("DELETE FROM ORDERS WHERE id = ?", id);

	}

	private Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		Order order = new Order();
		order.setId(rs.getInt("id"));
		order.setClient_id(rs.getInt("client_id"));
		order.setProduct_id(rs.getInt("product_id"));
		order.setPaid(rs.getBoolean("paid"));
		return order;
	}

	private Order mapRow2(ResultSet rs, int rowNum) throws SQLException {
		Order order = new Order();
		order.setId(rs.getInt("id"));
		order.setName(rs.getString("name"));
		order.setClient_id(rs.getInt("client_id"));
		order.setProduct_id(rs.getInt("product_id"));
		order.setPaid(rs.getBoolean("paid"));
		return order;
	}

}