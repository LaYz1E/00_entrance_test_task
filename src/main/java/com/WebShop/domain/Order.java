package com.WebShop.domain;

public class Order {
	private int id;
	private String name;

	private int client_id;
	private int product_id;
	private boolean paid;

	public Order(int id, int client_id, int product_id, boolean paid) {
		this.id = id;
		this.client_id = client_id;
		this.product_id = product_id;
		this.paid = paid;
	}

	public Order(int client_id, int product_id) {
		this.client_id = client_id;
		this.product_id = product_id;
	}

	public Order() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClient_id() {
		return client_id;
	}

	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", client_id=" + client_id + ", product_id=" + product_id + ", paid=" + paid + "]";
	}

}
