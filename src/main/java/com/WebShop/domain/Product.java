package com.WebShop.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Product {

	private int id;
	@NotNull
	@Size(min = 4, max = 45)
	private String name;
	@NotNull
	private int price;
	@Size(max = 100)
	private String specs;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getSpecs() {
		return specs;
	}

	public void setSpecs(String specs) {
		this.specs = specs;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + ", specs=" + specs + "]";
	}

}