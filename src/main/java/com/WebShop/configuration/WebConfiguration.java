package com.WebShop.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.WebShop.web.interceptor.AuthInterceptor;
import com.WebShop.web.interceptor.BlockedInterceptor;
import com.WebShop.web.interceptor.UserAddingHandlerInterceptor;

@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	private AuthInterceptor authInterceptor;

	@Autowired
	private UserAddingHandlerInterceptor userAddingHandlerInterceptor;

	@Autowired
	private BlockedInterceptor blockedInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(authInterceptor).addPathPatterns("/products/**");
		registry.addInterceptor(authInterceptor).addPathPatterns("/orders/**");
		registry.addInterceptor(authInterceptor).addPathPatterns("/clients/**");
		registry.addInterceptor(authInterceptor).addPathPatterns("/me/**");
		registry.addInterceptor(userAddingHandlerInterceptor).addPathPatterns("/**");
		registry.addInterceptor(blockedInterceptor).addPathPatterns("/products");
	}
}
