package com.WebShop.web.manager;

import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.WebShop.domain.Client;

@SessionScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class UserManager {

	private Client client;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
}
