package com.WebShop.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.WebShop.domain.Client;
import com.WebShop.service.ClientService;
import com.WebShop.web.manager.UserManager;

@Controller
public class AuthController {

	@Autowired
	private ClientService userService;

	@Autowired
	private UserManager userManager;

	@GetMapping({ "/", "/index" })
	public String index(Model model) {
		return "index";
	}

	@GetMapping("/clients")
	public String showClients(Model model) {
		if (userManager.getClient().isAdmin()) {
			List<Client> clients = userService.findAll();
			model.addAttribute("clients", clients);
			return "clients";
		} else
			return "index";
	}

	@GetMapping("/login")
	public String showLogin(Model model) {
		return "login";
	}

	@GetMapping("/blocked")
	public String showBlocked(Model model) {
		return "blocked";
	}

	@GetMapping("/admin")
	public String showAdmin(Model model) {
		return "admin";
	}

	@GetMapping("/register")
	public String showRegister(Model model) {
		return "register";
	}

	@GetMapping("/me")
	public String showMe(Model model) {
		if (userManager.getClient().isAdmin()) {
			return showClients(model);
		} else
			return "me";
	}

	@GetMapping(value = "/me", params = { "block" })
	public String block(Model model, @RequestParam(value = "block") int id) {
		if (userManager.getClient().isAdmin()) {
			userService.setBlock(id, true);
			return showClients(model);
		} else
			return "me";
	}

	@GetMapping(value = "/me", params = { "unblock" })
	public String unblock(Model model, @RequestParam(value = "unblock") int id) {
		if (userManager.getClient().isAdmin()) {
			userService.setBlock(id, false);
			return showClients(model);
		} else
			return "me";
	}

	@PostMapping("/login")
	public String authorize(@Valid Client user, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().forEach(System.out::println);
			return "login";
		}
		Client foundUser = userService.authenticate(user);
		if (foundUser != null) {
			userManager.setClient(foundUser);
			return "redirect:/index";
		} else {
			model.addAttribute("error", "bad data");
			return "/login";
		}
	}

	@PostMapping("/register")
	public String reg(@Valid Client user, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().forEach(System.out::println);
			return "login";
		}
		userService.save(user);
		return "redirect:/index";
	}

	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/index";
	}
}
