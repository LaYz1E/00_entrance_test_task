package com.WebShop.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.WebShop.domain.Order;
import com.WebShop.domain.Product;
import com.WebShop.service.OrderService;
import com.WebShop.service.ProductService;
import com.WebShop.web.manager.UserManager;

@Controller
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private ProductService productService;

	@Autowired
	private UserManager userManager;

	@GetMapping("/orders")
	public String listOrders(Model model) {
		List<Order> orders;
		if (userManager.getClient().isAdmin()) {
			orders = orderService.findAll();
		} else
			orders = orderService.findByClientID(userManager.getClient().getId());
		model.addAttribute("orders", orders);
		return "orders";
	}

	@GetMapping(value = "/orders", params = { "client" })
	public String listClientOrders(Model model, @RequestParam(value = "client") int id) {
		List<Order> orders;
		orders = orderService.findByClientID(id);
		model.addAttribute("orders", orders);
		return "orders";

	}

	@GetMapping(value = "/orders", params = { "delete" })
	public String deleteOrder(Model model, @RequestParam(value = "delete") int id) {
		if (userManager.getClient().isAdmin()) {
			orderService.deleteByID(id);
		}
		return listOrders(model);

	}

	@GetMapping(value = "/products", params = { "order" })
	public String makeOrder(Model model, @RequestParam(value = "order") int id) {
		orderService.save(new Order(userManager.getClient().getId(), id));
		List<Product> products = productService.findAll();
		model.addAttribute("products", products);
		return "products";
	}

	@GetMapping(value = "/orders", params = { "pay" })
	public String payOrder(Model model, @RequestParam(value = "pay") int id) {
		orderService.setPayByID(id, true);
		return this.listOrders(model);

	}

	@GetMapping(value = "/orders", params = { "unpay" })
	public String unpayOrder(Model model, @RequestParam(value = "unpay") int id) {
		orderService.setPayByID(id, false);
		return this.listOrders(model);

	}
}