package com.WebShop.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.WebShop.domain.Product;
import com.WebShop.service.ProductService;
import com.WebShop.web.manager.UserManager;

@Controller
public class ProductController {

	@Autowired
	private ProductService productService;
	@Autowired
	private UserManager userManager;

	@GetMapping("/products")
	public String listProducts(Model model) {
		List<Product> products = productService.findAll();
		model.addAttribute("products", products);
		return "products";
	}

	@GetMapping(value = "/products", params = { "delete" })
	public String deleteProduct(Model model, @RequestParam(value = "delete") int id) {
		if (userManager.getClient().isAdmin()) {
			productService.delete(id);
			return listProducts(model);
		} else
			return "products";
	}

	// "/products?id=${item.id}"
	@PostMapping(value = "/products", params = { "id" })
	public String editProduct(@Valid Product product, BindingResult bindingResult, @RequestParam(value = "id") int id) {
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().forEach(System.out::println);
			return "login";
		}
		if (userManager.getClient().isAdmin()) {
			productService.updateByID(id, product);
		}
		return "redirect:/products";
	}

	@PostMapping("/products")
	public String addProduct(@Valid Product product, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().forEach(System.out::println);
			return "login";
		}
		if (userManager.getClient().isAdmin()) {
			productService.save(product);
		}
		return "redirect:/products";
	}

}